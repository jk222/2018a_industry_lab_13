package ictgradschool.industry.lab13.ex01;

import ictgradschool.Keyboard;
import sun.awt.windows.ThemeReader;

public class RunnableExOne {

        public static void main(String[] args) throws InterruptedException {
             Thread threadOne = new Thread(new Runnable() {
                 @Override
                 public void run() {
                     for(int i = 0; i <= 1000000; i++){
                         if (Thread.currentThread().isInterrupted()) { return;}

                         System.out.println(i);
                     }
                 }
             });
            threadOne.start();
            Thread.sleep(500);
            threadOne.interrupt();

            try {
                threadOne.join();

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

        //OR:
    /*
    public class RunnableExOne {
        private static class MyRunnable implements Runnable {
        @Override
        public void run() {
            for(int i = 0; i < 1000000; i++){
                System.out.println(i);
            }
        }
    }

    public static void main(String[] args){
        Thread threadOne = new Thread(new MyRunnable());
        threadOne.start();
    }
    */
}





