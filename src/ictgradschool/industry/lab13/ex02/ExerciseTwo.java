package ictgradschool.industry.lab13.ex02;

import java.util.ArrayList;
import java.util.List;

public class ExerciseTwo {
    private int value = 0;

    private void start() throws InterruptedException {
        List<Thread> threads = new ArrayList<>();
        for (int i = 1; i < 100; i++) {
            final int toAdd = i;
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    add(toAdd);
                }
            });
            t.start();
            threads.add(t);
        }
        for (Thread t : threads) {
            t.join();
        }
        System.out.println("value = " + value);
    }

    private void add(int i) {
        value = value + i;
    }

    public static void main(String[] args) throws InterruptedException {
        new ExerciseTwo().start();
    }
}
/*
The output I expect from this program:
is for the value of the integers added to the arrayList 'threads', to
increment for each cycle of the for loop, by a value equal to integer i.
The program will stop at integer i = 100.
eg.
0+1=1
1+2=3
3+3=6
6+4=10
10+5=15
*/

/*
The output of the program is different from what I expected. As it produces a combined value final output of 4923.

 */
