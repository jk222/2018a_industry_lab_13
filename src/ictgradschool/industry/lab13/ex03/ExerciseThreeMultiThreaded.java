package ictgradschool.industry.lab13.ex03;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 */
public class ExerciseThreeMultiThreaded extends ExerciseThreeSingleThreaded {

    private class myPIRunnable implements Runnable {
        public double results;
        private final long iterations;

        private myPIRunnable(long iterations){
            this.iterations = iterations;
        }

        @Override
        public void run(){
            results = ExerciseThreeMultiThreaded.super.estimatePI(iterations);
        }

    }

    /**
     * Estimates PI using a multi-threaded Monte Carlo method.
     */
    @Override
    protected double estimatePI(long numSamples) {

        // numSamples = 1_000_000_000L; //can be edited out later
        final int numberOfThreads = 500;

        List<myPIRunnable> runnablesList = new ArrayList<>();
        List<Thread> threadsList = new ArrayList<>();

        for (int i = 0; i < numberOfThreads; i++ ){
            myPIRunnable piRunnable01 = new myPIRunnable(numSamples/numberOfThreads);
            Thread thread01 = new Thread(piRunnable01);

            runnablesList.add(piRunnable01);
            threadsList.add(thread01);

            thread01.start();

        }

        double sum =0;

        for (int i = 0; i < threadsList.size(); i++){
            try {
                threadsList.get(i).join();
                sum += runnablesList.get(i).results;

            }catch (InterruptedException e) {
                e.printStackTrace();

            }
        }

        return sum/numberOfThreads;
    }

    /** Program entry point. */
    public static void main(String[] args) {
        new ExerciseThreeMultiThreaded().start();
    }
}
